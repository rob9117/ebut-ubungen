<%@ page session="true" import="de.htwg_konstanz.ebus.wholesaler.demo.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>eBusiness Framework Demo - XML Upload</title>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <link rel="stylesheet" type="text/css" href="default.css">
</head>
<body>

<%@ include file="header.jsp" %>
<%@ include file="error.jsp" %>
<%@ include file="authentication.jsp" %>
<%@ include file="navigation.jspfragment" %>

<h1>XML Uplaod</h1>
<div>
    <form name="xmlUploadForm" method="post"
          action="<%= response.encodeURL("controllerservlet?action=" + Constants.ACTION_SHOW_XML_UPLOAD)%>"
          enctype="multipart/form-data">
        <input type="file" name="<%= Constants.PARAM_XML_FILE%>">
        <input type="submit" value="Submit">
    </form>
    <p style="color: green">
        <% String name = (String) request.getAttribute("xmlImported");
            if (name != null) {

                out.println(name);
            }
        %>
    </p>
</div>
</body>
</html>
