<?xml version="1.0" encoding="UTF-8"?>
	<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" indent="yes"/>
		<xsl:template match="/">
			<html xml:lang="en" lang="en">
				<head>
					<title><xsl:value-of select="BMECAT/HEADER/CATALOG/CATALOG_NAME"/></title>
				</head>
				<body>			
					<xsl:apply-templates select="BMECAT"/>
				</body>
			</html>
		</xsl:template>

		<xsl:template match="HEADER" name="header">
			<h1>Catalog-Name: <xsl:value-of select="//CATALOG_NAME"/></h1>
			<h2>Supplier-Name: <xsl:value-of select="//SUPPLIER_NAME"/></h2>
		</xsl:template>

		<xsl:template match="T_NEW_CATALOG" name="catalog">
			<table border="1">
					<tr>
						<th>Order-Details</th>
						<th>Titel</th>
						<th>Description</th>
						<th>Price net</th>
						<th>Price gross</th>
						<th>Territory</th>
					</tr>
				<xsl:for-each select="ARTICLE">
					<tr>
						<td><xsl:value-of select="concat(ARTICLE_ORDER_DETAILS/ORDER_UNIT, '-', ARTICLE_ORDER_DETAILS/CONTENT_UNIT, '-', ARTICLE_ORDER_DETAILS/NO_CU_PER_OU)"/></td>
						<td><xsl:value-of select="ARTICLE_DETAILS/DESCRIPTION_SHORT"/></td>
						<td><xsl:value-of select="ARTICLE_DETAILS/DESCRIPTION_LONG"/></td>

						<xsl:variable name="netprice" select="ARTICLE_PRICE_DETAILS[1]/ARTICLE_PRICE/PRICE_AMOUNT"/>
						<xsl:variable name="tax" select="ARTICLE_PRICE_DETAILS[1]/ARTICLE_PRICE/TAX"/>
						<td><xsl:value-of select="$netprice"/></td>
						<td><xsl:value-of select="($netprice*(1+$tax))"/></td>

						<td><xsl:value-of select="ARTICLE_PRICE_DETAILS/ARTICLE_PRICE/TERRITORY"/></td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:template>
	</xsl:stylesheet>
