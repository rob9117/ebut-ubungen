/* ProductListAction.java
***********************************************************************************
* 15.03.2007 ** tdi
* - created
*
***********************************************************************************
* Copyright 2007 HTWG Konstanz
*
* Prof. Dr.-Ing. Juergen Waesch
* Dipl. -Inf. (FH) Thomas Dietrich
* Fakultaet Informatik - Department of Computer Science
* E-Business Technologien
*
* Hochschule Konstanz Technik, Wirtschaft und Gestaltung
* University of Applied Sciences
* Brauneggerstrasse 55
* D-78462 Konstanz
*
* E-Mail: juergen.waesch(at)htwg-konstanz.de
************************************************************************************/
package de.htwg_konstanz.ebus.wholesaler.action;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import de.htwg_konstanz.ebus.wholesaler.demo.*;
import de.htwg_konstanz.ebus.wholesaler.demo.util.Constants;
import de.htwg_konstanz.ebus.wholesaler.main.Import;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.w3c.dom.*;

/**
 * The ProductListAction loads all available products from the database.<p>
 * After loading, the action puts all products into an List-Object and makes them
 * available for the corresponding view (JSP-Page) via the HTTPSession.
 *
 * @author tdi
 */
public class ImportAction implements IAction {

    public ImportAction() {
        super();
    }

    /**
     * The execute method is automatically called by the dispatching sequence of the {@link ControllerServlet}.
     *
     * @param request   the HttpServletRequest-Object provided by the servlet engine
     * @param response  the HttpServletResponse-Object provided by the servlet engine
     * @param errorList a Stringlist for possible error messages occured in the corresponding action
     * @return the redirection URL
     */
    public String execute(HttpServletRequest request, HttpServletResponse response, ArrayList<String> errorList) {
        errorList.clear();
        Document document = null;
        final String importPage = "xmlUpload.jsp";
        LoginBean loginBean = (LoginBean) request.getSession(true).getAttribute(Constants.PARAM_LOGIN_BEAN);

        if (loginBean == null && !loginBean.isLoggedIn()) {
            System.out.println("redirect to login");
            return "login.jsp";
        }

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        Import importHelper = new Import(request, errorList);

        DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
        ServletFileUpload servletFileUpload = new ServletFileUpload(diskFileItemFactory);
        try {
            List<FileItem> fileItems = servletFileUpload.parseRequest(request);
            document = importHelper.importXML(fileItems);
        } catch (FileUploadException e) {
            e.printStackTrace();
        }

        if(document == null) return importPage;

        SaveImport saveImport = new SaveImport(document, errorList);

        saveImport.saveArticles();
        if (!isMultipart) {
            errorList.add("request was no multipart");
            return importPage;
        }

        return importPage;
    }


    /**
     * Each action itself decides if it is responsible to process the corrensponding request or not.
     * This means that the {@link ControllerServlet} will ask each action by calling this method if it
     * is able to process the incoming action request, or not.
     *
     * @param actionName the name of the incoming action which should be processed
     * @return true if the action is responsible, else false
     */
    public boolean accepts(String actionName) {
        return actionName.equalsIgnoreCase(Constants.ACTION_SHOW_XML_UPLOAD);
    }

}
