package de.htwg_konstanz.ebus.wholesaler.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import de.htwg_konstanz.ebus.framework.wholesaler.api.bo.BOProduct;
import de.htwg_konstanz.ebus.framework.wholesaler.api.bo.BOSalesPrice;
import de.htwg_konstanz.ebus.framework.wholesaler.api.boa.ProductBOA;
/**
 * This class provides an export functionality for a servlet request.
 * It is able to create XML documents in BMECAT and XHTML out of the product catalog.
 * 
 * @author Hamster
 *
 */
public class Export {

	private HttpServletRequest request;
	private HttpServletResponse response;
	ServletContext context;
	private ArrayList<String> errorList;

	public Export(HttpServletRequest request, HttpServletResponse response, ArrayList<String> errorList){
		this.request = request;
		this.response = response;
		this.errorList = errorList;
		this.context = request.getSession().getServletContext();
	}

	
	/**
	 * Starts the export logic. Reads request parameters and decides which case to execute.
	 * Cases are to show xml or xhtml, or to donwload xml or xhtml. Selected requestParameter are "exportFormat" and "search".
	 * Values for "exportFormat" are "xml", "xhtml", "xmlDownload" or "xhtmlDownload".
	 * @param userId user name
	 * @return path to exported file
	 */
	public String exportFile(String userId){
		String exportFormat = this.request.getParameter("exportFormat");
		String search = this.request.getParameter("search");
		String tmpPath, filePath = null; 
		System.out.println(exportFormat);
		switch(exportFormat){
			case "xml":
					filePath = this.writeDomIntoFile(userId, search);
				break;
			case "xhtml":
					tmpPath = this.writeDomIntoFile(userId, search);
					filePath = this.transformCatalogToXhtml(tmpPath, userId);
				break;
			case "xmlDownload":
					filePath = this.writeDomIntoFile(userId, search);
					this.downloadFile(filePath);
				break;
			case "xhtmlDownload":
					tmpPath = this.writeDomIntoFile(userId, search);
					filePath = this.transformCatalogToXhtml(tmpPath, userId);
					this.downloadFile(filePath);
				break;
			default:
				break;
		}
		return filePath;
	}

	public String exportFile(String userId, String exportFormat, String search){
		String tmpPath, filePath = null;
		System.out.println(exportFormat);
		switch(exportFormat){
			case "xml":
				filePath = this.writeDomIntoFile(userId, search);
				break;
			case "xhtml":
				tmpPath = this.writeDomIntoFile(userId, search);
				filePath = this.transformCatalogToXhtml(tmpPath, userId);
				break;
			case "xmlDownload":
				filePath = this.writeDomIntoFile(userId, search);
				this.downloadFile(filePath);
				break;
			case "xhtmlDownload":
				tmpPath = this.writeDomIntoFile(userId, search);
				filePath = this.transformCatalogToXhtml(tmpPath, userId);
				this.downloadFile(filePath);
				break;
			default:
				break;
		}
		return filePath;
	}

	
	/**
	 * Getter for errorList property.
	 * @return ArrayList contains error messages
	 */
	public ArrayList<String> getErrorList() {
		return errorList;
	}

	/**
	 * Exports the whole product catalog, or a selection matching the search parameter.
	 * Returns the result as a document.
	 * @param search String, could be empty or null
	 * @return document to export
	 */
	public Document exportCatalog(String search){
		Document document = null;
		if(!search.isEmpty() && search != null)
			document = this.createCatalog(search);
		else 
			document = this.createCatalog();
		return document;
	}
	
	/**
	 * Creates the document using the exportCatalog-function. Inserts the DOM into a XML document.
	 * @param userId user name
	 * @param search if empty or null whole catalog will be selected
	 * @return path to the exported XML file
	 */
	protected String writeDomIntoFile(String userId, String search){
		Document document = this.exportCatalog(search);
		String path="catalog_export_"+ userId + ".xml";
		File file=null;
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			file = new File(this.context.getRealPath(path));
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			this.errorList.add("Configuration Error during transforming: " + e.getMessage());
			e.printStackTrace();
		} catch (TransformerException e) {
			this.errorList.add("Error during transforming: " + e.getMessage());
			e.printStackTrace();
		}
		return path;
	}
	
	/**
	 * Transforms a bmecat valid xml-file into an xhtml-file using the xslt-transformation. 
	 * @param pathToTransform path of XML file to transform
	 * @param userId user name
	 * @return path to transformed File
	 */
	private String transformCatalogToXhtml(String pathToTransform, String userId) {
		String path ="catalog_export_" + userId + ".XHTML";
		File file = new File(this.context.getRealPath(path));
		String xmlPath = this.context.getRealPath(pathToTransform);
		String xsltPath = this.context.getRealPath("/wsdl/transform_to_xhtml_v2.xslt");
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer;
			StreamSource xmlSource = new StreamSource(xmlPath);
			StreamSource xsltSource = new StreamSource(xsltPath);
			transformer = factory.newTransformer(xsltSource);
			transformer.transform(xmlSource, new StreamResult(file));
		} catch (TransformerConfigurationException e) {
			this.errorList.add("Configuration error during transforming file: " + e.getMessage());
			e.printStackTrace();
		} catch (TransformerException e) {
			this.errorList.add("Error during transforming file: " + e.getMessage());
			e.printStackTrace();
		}
		return path;
	}
	
	/**
	 * Creates the catalog out of the product business objects (db). 
	 * All products will appear in the catalog
	 * @return document
	 */
	public Document createCatalog(){
		Document document = null;
		List<BOProduct> productList = ProductBOA.getInstance().findAll();
		try {
			document = this.getDocument();
			this.createDomInDocument(document);
		} catch (ParserConfigurationException e) {
			this.errorList.add("Error while creating the product catalog: " + e.getMessage());
			e.printStackTrace();
		}
		for(BOProduct iter: productList){
			this.appendArticles(document, iter);
		}
		return document;
	}
	
	/**
	 * Overload of the createCatalog method. 
	 * The catalog will contain those products the short description matches the search string.
	 * @param search to match the short description of products
	 * @return document
	 */
	public Document createCatalog(String search){
		Document document = null;
		String checkString;
		Boolean valueExist = false;
		List<BOProduct> productList = ProductBOA.getInstance().findAll();
		try {
			document = this.getDocument();
			this.createDomInDocument(document);
		} catch (ParserConfigurationException e) {
			this.errorList.add("Error while creating the product catalog: " + e.getMessage());
			e.printStackTrace();
		}
		for(BOProduct iter: productList){
			checkString = iter.product.getShortdescription();
			if(search != null && !search.isEmpty() && checkString.toLowerCase().contains(search.toLowerCase())){
				valueExist = true;
				this.appendArticles(document, iter);
			}
		}
		if(!valueExist)
			errorList.add("No article with the short description '" + search + "' found.");
			
		return document;
	}
	
	/**
	 * Returns a new document.
	 * @return new Document
	 * @throws ParserConfigurationException
	 */
	private Document getDocument() throws ParserConfigurationException{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();
		return document;
	}
	
	/**
	 * Creates the basic BMECAT DOM structure into the document.
	 * @param document to insert
	 * @throws ParserConfigurationException
	 */
	private void createDomInDocument(Document document) throws ParserConfigurationException{
		// get root element 
		Element root = document.createElement("BMECAT");
		root.setAttribute("version", "1.2");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		//child element declaration
		Element header = document.createElement("HEADER");
		Element catalog = document.createElement("CATALOG");
		Element language = document.createElement("LANGUAGE");
		Element catalog_id = document.createElement("CATALOG_ID");
		Element catalog_version = document.createElement("CATALOG_VERSION");
		Element catalog_name = document.createElement("CATALOG_NAME");
		Element supplier = document.createElement("SUPPLIER");
		Element supplier_name = document.createElement("SUPPLIER_NAME");
		Element t_new_catalog = document.createElement("T_NEW_CATALOG");
		//append elements and value insertion		
		document.appendChild(root);
		root.appendChild(header);
		header.appendChild(catalog);
		catalog.appendChild(language);
		language.insertBefore(document.createTextNode("deu"), language.getLastChild());
		catalog.appendChild(catalog_id);
		catalog_id.insertBefore(document.createTextNode("EBUT-SS16"), catalog_id.getLastChild());
		catalog.appendChild(catalog_version);
		catalog_version.insertBefore(document.createTextNode("1.0"), catalog_version.getLastChild());
		catalog.appendChild(catalog_name);
		catalog_name.insertBefore(document.createTextNode("Exportierter Produktkatalog"), catalog_name.getLastChild());
		header.appendChild(supplier);
		supplier.appendChild(supplier_name);
		supplier_name.insertBefore(document.createTextNode("KN MEDIA STORE"), supplier_name.getLastChild());
		root.appendChild(t_new_catalog);
	}
	
	/**
	 * Appends an article to the documents "T_NEW_CATALOG"-element, extracting data from the database (business object BOProduct)
	 * @param document to append articles
	 * @param product business object
	 */
	private void appendArticles(Document document, BOProduct product){
		// get start node
		Node t_new_catalog = document.getElementsByTagName("T_NEW_CATALOG").item(0);
		// declaration of elements 
		Element article = document.createElement("ARTICLE");
		Element supplier_aid = document.createElement("SUPPLIER_AID");
		Element article_details = document.createElement("ARTICLE_DETAILS");
		Element description_short = document.createElement("DESCRIPTION_SHORT");
		Element description_long = document.createElement("DESCRIPTION_LONG");
		Element article_order_details = document.createElement("ARTICLE_ORDER_DETAILS");
		Element order_unit = document.createElement("ORDER_UNIT");
		Element content_unit = document.createElement("CONTENT_UNIT");
		Element no_cu_per_ou = document.createElement("NO_CU_PER_OU");
		Element article_price_details = document.createElement("ARTICLE_PRICE_DETAILS");
		// append elements and values
		t_new_catalog.appendChild(article);
		supplier_aid.insertBefore(document.createTextNode(product.getOrderNumberSupplier()), supplier_aid.getLastChild());
		article.appendChild(supplier_aid);
		article.appendChild(article_details);
		description_short.insertBefore(document.createTextNode(product.getShortDescription()), description_short.getLastChild());
		article_details.appendChild(description_short);
		description_long.insertBefore(document.createTextNode(product.getLongDescription()), description_long.getLastChild());
		article_details.appendChild(description_long);
		article.appendChild(article_order_details);
		order_unit.insertBefore(document.createTextNode("PK"), order_unit.getLastChild());
		article_order_details.appendChild(order_unit);
		content_unit.insertBefore(document.createTextNode("C62"), content_unit.getLastChild());
		article_order_details.appendChild(content_unit);
		no_cu_per_ou.insertBefore(document.createTextNode("10"), no_cu_per_ou.getLastChild());
		article_order_details.appendChild(no_cu_per_ou);
		article.appendChild(article_price_details);
			
		this.appendArticlePriceDetails(document, article_price_details, product);	
	}
	
	/**
	 * Appends the "ARTICLE_PRICE_DETAILS"-tag into an article, extracting implied data from the business object parameter.
	 * @param element represents the "ARTICLE_PRICE_DETAILS" Element
	 * @param product business object to get price information from the database
	 */
	private void appendArticlePriceDetails(Document document, Element element, BOProduct product){
		Element article_price_details = element;
		List<BOSalesPrice> salesPrices = product.getSalesPrices();
		// iteration all products
		for(BOSalesPrice salePrice : salesPrices){
			// declaration of elements
			Element article_price = document.createElement("ARTICLE_PRICE");
			Element price_amount = document.createElement("PRICE_AMOUNT");
			Element price_currency = document.createElement("PRICE_CURRENCY");
			Element tax = document.createElement("TAX");
			Element territory = document.createElement("TERRITORY");
			// append elements and values
			article_price.setAttribute("price_type", salePrice.getPricetype());
			article_price_details.appendChild(article_price);
			article_price.appendChild(price_amount);
			price_amount.insertBefore(document.createTextNode(salePrice.getAmount().toString()), price_amount.getLastChild());		
			article_price.appendChild(price_currency);
			price_currency.insertBefore(document.createTextNode(salePrice.getCountry().getCurrency().getCode()), price_currency.getLastChild());
			article_price.appendChild(tax);
			tax.insertBefore(document.createTextNode(salePrice.getTaxrate().toString()), tax.getLastChild());
			article_price.appendChild(territory);
			territory.insertBefore(document.createTextNode(salePrice.getCountry().getIsocode()), territory.getLastChild());
		}
	}
	/**
	 * Starts the donwload of the file from the path parameter.
	 * @param path of the file to donwload
	 */
	private void downloadFile(String path){
		try {
			this.response.setContentType("text/xml");
			this.response.setHeader("Content-Disposition", "attachment;filename="+path);
			File file = new File(this.context.getRealPath(path));
			FileInputStream fileIn = new FileInputStream(file);
			ServletOutputStream out = this.response.getOutputStream();
			int bytesRead;
			byte[] bytes = new byte[4096];
			// copy binary content to output stream
			while ((bytesRead = fileIn.read(bytes)) != -1) {
			    out.write(bytes, 0, bytesRead);
			}
			fileIn.close();
			out.flush();
			out.close();
		} catch (IOException e) {
			this.errorList.add("Error while creating file: " + e.getMessage());
			e.printStackTrace();
		}	
	}
}