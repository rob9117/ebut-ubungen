package de.htwg_konstanz.ebus.wholesaler.main;

import de.htwg_konstanz.ebus.wholesaler.action.SaveImport;
import de.htwg_konstanz.ebus.wholesaler.demo.util.Constants;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Import {

    private static boolean isXMLFile = false;
    private static boolean isNoFile = false;
    HttpServletRequest request;
    ArrayList<String> errorList;

    public Import(HttpServletRequest request, ArrayList<String> errorList) {
        this.request = request;
        this.errorList = errorList;
    }

    public Import () {}

    /**
     * reads XML from request by calling another function and passing document
     * to validate under given schema filepath. This method returns a valid document
     * @return Document
     */
    public Document importXML(List<FileItem> fileItems) {

        ServletContext servletContext = request.getSession().getServletContext();
        String schemaFilePath = servletContext.getRealPath("/wsdl/bmecat_new_catalog_1_2_simple_without_NS.xsd");

        Document document = getDocumentFromXML(fileItems, errorList);

        boolean isValid;
        try {
            isValid = validateWithXSD(schemaFilePath, document, errorList);
        } catch (IOException e) {
            errorList.add("Document not well-formed: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        if (!isXMLFile && !isNoFile) {
            errorList.add("File upload error: Wrong document type");
            return document;
        }
        if (!isValid) {
            errorList.add("File is not valid");
            return document;
        }
        if (isNoFile) {
            errorList.add("No file uploaded");
            return document;
        }

        return document;
    }

    /**
     * checks if uploaded file is a XML file and returns document, else returns null
     *
     * @param fileItems
     * @param errorList
     * @return Document
     */
    public Document getDocumentFromXML(List<FileItem> fileItems, ArrayList<String> errorList) {
        Document document;
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        isNoFile = fileItems.iterator().next().getName().isEmpty();
        isXMLFile = fileItems.iterator().next().getName().toLowerCase().endsWith(".xml");

        document = getDocumentFromFileItems(fileItems, documentBuilderFactory, errorList);
        return document;
    }

    /**
     * validates document with XSD and prints exception when XML file is not valid.
     * @param filepath
     * @param document
     * @param errorList
     * @return boolean
     * @throws IOException
     */
    public boolean validateWithXSD(String filepath, Document document, ArrayList<String> errorList) throws IOException {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            File file = new File(filepath);
            Schema schema = schemaFactory.newSchema(file);
            Validator validator = schema.newValidator();
            validator.validate(new DOMSource(document));
        } catch (SAXException e) {
            errorList.add("XSD Validator error: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * this method file data from request and checks if data was sent.
     * Prints errors when no file was uploaded or
     * file is not readable.
     * @param fileItems
     * @param documentBuilderFactory
     * @param errorList
     * @return
     */
    public static Document getDocumentFromFileItems(List<FileItem> fileItems, DocumentBuilderFactory documentBuilderFactory, ArrayList<String> errorList) {
        Document document = null;
        for (FileItem fileItem : fileItems) {
            InputStream inputStream;
            DocumentBuilder documentBuilder;
            try {
                inputStream = fileItem.getInputStream();
                documentBuilder = documentBuilderFactory.newDocumentBuilder();
                document = documentBuilder.parse(inputStream);
            } catch (SAXException e) {
                if (isXMLFile) {
                    errorList.add("File upload error: " + e.getMessage());
                }
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                errorList.add("File upload error:" + e.getMessage());
                e.printStackTrace();
                return null;
            } catch (ParserConfigurationException e) {
                errorList.add("File upload error:" + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
        return document;
    }
}
