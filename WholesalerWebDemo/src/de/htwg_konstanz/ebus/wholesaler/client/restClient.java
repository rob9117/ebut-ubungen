package de.htwg_konstanz.ebus.wholesaler.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;
import com.sun.org.apache.xpath.internal.operations.String;
import org.apache.commons.fileupload.FileItem;

import javax.ws.rs.core.MediaType;
import java.io.File;

/**
 * Created by Hamster on 21.06.16.
 */
public class restClient {
    public static void main(String[] args) {
        try{
            doExport();
            doImport();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private static void doExport() {
        Client client = Client.create();

        WebResource webResource = client.resource("http://localhost:8080/rest/export/");

        ClientResponse resp = webResource.accept(MediaType.TEXT_XML_TYPE).get(ClientResponse.class);

        if(resp.getStatus() != 200){
            throw new RuntimeException("Failed: HTTP error code: " + resp.getStatus());
        }

        String out = resp.getEntity(String.class);
        System.out.println("Output: ");
        System.out.println(out);
        client.destroy();
    }

    private static void doImport() {
        DefaultClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource webResource = client.resource("http://localhost:8080/rest/import/fileupload");
        File file = new File("catalog_export_1,xml");

        if(file != null) {
            Form form = new Form();
            form.add("file", file);
            System.out.println(form);
            FileItem response = webResource.type(MediaType.MULTIPART_FORM_DATA).post(FileItem.class, file);
            System.out.println(response);
        }


        client.destroy();
    }
}
