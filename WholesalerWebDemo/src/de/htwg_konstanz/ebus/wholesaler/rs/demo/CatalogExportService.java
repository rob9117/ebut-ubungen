package de.htwg_konstanz.ebus.wholesaler.rs.demo;

import de.htwg_konstanz.ebus.wholesaler.main.Export;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

@Path("/export")
public class CatalogExportService {

    @GET
    @Path("/{param}")
    public Response getCatalog(@PathParam("param") String search, @Context HttpServletRequest request, @Context HttpServletResponse response){
        Export exporter = new Export(request, response, new ArrayList<String>(){});
        String path = exporter.exportFile("1", "xmlDownload", search);
        return Response.status(200).entity(path).build();
    }

    @GET
    @Path("/")
    public Response getCatalog(@Context HttpServletRequest request, @Context HttpServletResponse response){
        Export exporter = new Export(request, response, new ArrayList<String>(){});
        String path = exporter.exportFile("1", "xmlDownload", "");
        return Response.status(200).entity(path).build();
    }
}
