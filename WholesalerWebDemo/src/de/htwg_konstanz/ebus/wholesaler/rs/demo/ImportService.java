package de.htwg_konstanz.ebus.wholesaler.rs.demo;

import de.htwg_konstanz.ebus.wholesaler.action.SaveImport;
import de.htwg_konstanz.ebus.wholesaler.main.Import;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.w3c.dom.Document;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
//import javax.ws.rs.core.*;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/import")
public class ImportService {

    @POST
    @Path("/fileupload")
    @Consumes("multipart/form-data")
    public Response importFile(@Context HttpServletRequest request,
                               @Context HttpServletResponse response) {
        System.out.println();
        ArrayList<String> errorList = new ArrayList<String>();
        DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
        ServletFileUpload servletFileUpload = new ServletFileUpload(diskFileItemFactory);
        try {
            System.out.println(request.getParameter("file"));

            List<FileItem> fileItems = servletFileUpload.parseRequest(request);
            Document document = new Import().getDocumentFromXML(fileItems, errorList);
            System.out.println(fileItems.iterator().next().getName());
            new SaveImport(document, errorList).saveArticles();

        } catch (FileUploadException e) {
            e.printStackTrace();
        }
        return Response.status(200).entity(errorList).build();
    }


}
