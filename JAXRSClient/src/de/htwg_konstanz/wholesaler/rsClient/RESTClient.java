package de.htwg_konstanz.wholesaler.rsClient;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class RESTClient {

	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
		
		// Trivial get
		String request = "Give me this text back";
		Response res = client.target("http://localhost:8080/rest/hello/"+request)
				.request(MediaType.TEXT_PLAIN)
				.get();
		
		String response = res.readEntity(String.class);
		System.out.println(response);
		
		// Easy get
		int num = 2;
		Response res2 = client.target("http://localhost:8080/rest/hello/square/"+num)
				.request(MediaType.TEXT_PLAIN)
				.get();
		
		String response2 = res2.readEntity(String.class);
		System.out.println(response2);
		
		// Easy post		
		Response res3 = client.target("http://localhost:8080/rest/hello/squareroot")
				.request(MediaType.TEXT_PLAIN)
				.post(Entity.entity(25, MediaType.TEXT_PLAIN));
		String response3 = res3.readEntity(String.class);
		System.out.println(response3);
	}

}
